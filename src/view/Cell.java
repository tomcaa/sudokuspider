package view;

import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by tomcaa on 12/9/17.
 */
public class Cell extends StackPane {

    private SmallLabel[] tempFields = new SmallLabel[9];
    private LargeLabel finalField;
    private GridPane smallFieldPane;
    private Field parent;
    private boolean finalFieldIsActive = false;


    private int row, col;

    private Cell() {
        super();
        getStyleClass().add("sudoku-cell");
        smallFieldPane = new GridPane();
        smallFieldPane.getStyleClass().add("small-field-pane");
        finalField = new LargeLabel("0", this);
        getChildren().add(smallFieldPane);
    }

    private Cell(int row, int col) {
        this();
        this.row = row;
        this.col = col;
        // border:
        int top = 1;
        int right = 1;
        int bottom = 1;
        int left = 1;

        if (row % 3 == 0) {
            top = 2;
        }
        if (row % 3 == 2) {
            bottom = 2;
        }
        if (col % 3 == 0) {
            left = 2;
        }
        if (col % 3 == 2) {
            right = 2;
        }

        setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, null, new BorderWidths(top, right, bottom, left))));
    }

    static Cell withDefaultFields(int row, int col, Field parent) {
        Cell c = new Cell(row, col);
        c.initSmallFieldPane();
        c.parent = parent;
        return c;
    }

    static Cell deserialize(ObjectInputStream stream, Field parent) throws IOException, ClassNotFoundException {
        int row = stream.read();
        int col = stream.read();
        Cell c = Cell.withDefaultFields(row, col, parent);
        for (int i = 0; i < 9; i++) {
            c.tempFields[i].setState((State) stream.readObject());
        }
        c.finalFieldIsActive = stream.readBoolean();
        c.finalField.setState((State) stream.readObject());
        c.finalField.setText(stream.readUTF());
        if (c.isFinalFieldActive()) {
            c.activateLargeField();
        } else {
            c.activateSmallFields();
        }
        return c;
    }

    private void initSmallFieldPane() {
        for (int i = 0; i < 9; i++) {
            tempFields[i] = new SmallLabel(Integer.toString(i + 1), this);
            smallFieldPane.add(tempFields[i], i % 3, i / 3);
        }
    }

    void activateSmallFields() {
        getChildren().clear();
        getChildren().add(smallFieldPane);
        finalFieldIsActive = false;
    }

    private void activateLargeField() {
        getChildren().clear();
        getChildren().add(finalField);
        finalFieldIsActive = true;
    }

    void setValue(int value, boolean isFinal) {
        activateLargeField();
        finalField.setText(Integer.toString(value));
        if (isFinal) {
            finalField.setState(State.BLACK);
        } else {
            finalField.setState(State.BLUE);
        }
    }

    private void deactivateValue(int value) {
        if (value > 0) {
            SmallLabel field = tempFields[value - 1];
            field.setState(State.DISABLED);
        }
    }

    private boolean isActiveValue(int value) {
        if (value > 0) {
            SmallLabel field = tempFields[value - 1];
            return !field.getState().equals(State.DISABLED);
        }
        return false;
    }

    void deactivateIfActive(int value) {
        if (isActiveValue(value))
            deactivateValue(value);
    }

    void autoActivate() {
        int active = 0;
        for (int i = 0; i < 9; i++) {
            if (isActiveValue(i + 1)) {
                if (active == 0) {
                    active = i + 1;
                } else {
                    return;
                }
            }
        }
        if (active != 0) {
            setValue(active, false);
        }
    }

    int getActiveValue() {
        if (finalFieldIsActive) {
            return Integer.parseInt(finalField.getText());
        }
        return 0;
    }

    boolean isFinalFieldActive() {
        return finalFieldIsActive;
    }

    public Cell clone(Field parent) {
        Cell clone = Cell.withDefaultFields(row, col, parent);
        clone.finalField.setText(finalField.getText());
        clone.finalField.setState(finalField.getState());
        for (int i = 0; i < 9; i++)
            clone.tempFields[i].setState(tempFields[i].getState());
        if (finalFieldIsActive)
            clone.activateLargeField();
        return clone;
    }

    void serialize(ObjectOutputStream stream) throws IOException {
        stream.write(row);
        stream.write(col);
        for (int i = 0; i < 9; i++) {
            stream.writeObject(tempFields[i].getState());
        }
        stream.writeBoolean(isFinalFieldActive());
        stream.writeObject(finalField.getState());
        stream.writeUTF(finalField.getText());
    }

    void propagateMarkedEvent(String value, boolean isSource) {
        for (SmallLabel lbl : tempFields) {
            lbl.switchMarkedState(value);
        }
        if (isSource) {
            parent.propagateMarkedEvent(value, this);
        }
    }

}