package view;

import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by tomcaa on 12/9/17.
 */
public class Field extends GridPane implements Serializable {

    public Cell[][] cells = new Cell[9][9];


    private Field() {
        super();
    }

    private Field(Field f) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cells[i][j] = f.cells[i][j].clone(this);
                add(cells[i][j], j, i);
            }
        }
    }

    public static Field withDefaultCells() {
        Field f = new Field();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                f.cells[i][j] = Cell.withDefaultFields(i, j, f);
                f.add(f.cells[i][j], j, i);
            }
        }
        return f;
    }

    public static Field deserialize(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        Field f = new Field();
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                f.cells[i][j] = Cell.deserialize(stream, f);
                f.add(f.cells[i][j], j, i);
            }
        }
        return f;
    }

    public void clearFieldsForCell(int row, int col) {
        int value = cells[row][col].getActiveValue();
        if (value == 0)
            return;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                // same row
                if (i == row && j != col) {
                    cells[i][j].deactivateIfActive(value);
                }
                // same col
                else if (i != row && j == col) {
                    cells[i][j].deactivateIfActive(value);
                }
                // same square
                else if (i / 3 == row / 3 && col / 3 == j / 3) {
                    cells[i][j].deactivateIfActive(value);
                }
            }
        }
    }

    public Field clone() {
        return new Field(this);
    }

    public void serialize(ObjectOutputStream stream) throws IOException {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                cells[i][j].serialize(stream);
            }
        }
    }

    public void propagateMarkedEvent(String value, Cell source) {
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (!source.equals(cells[i][j]))
                    cells[i][j].propagateMarkedEvent(value, false);
    }
}
