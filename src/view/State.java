package view;

import java.io.Serializable;

/**
 * Created by tomcaa on 12/9/17.
 */
public enum State implements Serializable {
    ENABLED, DISABLED, BLUE, RED, BLACK;
}
