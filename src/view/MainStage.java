package view;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.Main;

import java.io.*;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by tomcaa on 12/9/17.
 */
public class MainStage extends Stage {

    BorderPane root = new BorderPane();
    Field field;

    Stack<Field> stanja = new Stack<>();

    public MainStage() {
        super();
        Field rootPane = Field.withDefaultCells();
        this.field = rootPane;
        setTitle("Sudoku Spider");

        Menu main = new Menu("Sudoku Spider");
        MenuItem newSudoku = new MenuItem("Unesi sudoku");
        MenuItem loadSudoku = new MenuItem("Učitaj sudoku");

        MenuItem exportSudoku = new MenuItem("Izvezi sudoku");
        MenuItem importSudoku = new MenuItem("Uvezi sudoku");

        MenuItem reloadCss = new MenuItem("Osveži stil");

        newSudoku.setOnAction(e -> {
            new NewSudokuStage(this).showAndWait();
        });

        loadSudoku.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            fc.setTitle("Otvori Sudoku");
            File f = fc.showOpenDialog(MainStage.this);
            if (f != null) {
                try {
                    Scanner sc = new Scanner(f);
                    StringBuilder sb = new StringBuilder();
                    while (sc.hasNextLine()) {
                        sb.append(sc.nextLine());
                    }
                    sc.close();
                    this.loadSudoku(sb.toString());
                } catch (IOException ex) {
                    new Alert(Alert.AlertType.ERROR, "Greška u čitanju datoteke!", ButtonType.OK).show();
                }
            }
        });

        exportSudoku.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("[*.sud] - Spider Sudoku fajlovi", "*.sud");
            fc.getExtensionFilters().add(filter);
            fc.setSelectedExtensionFilter(filter);
            fc.setTitle("Izvezi Sudoku");
            File f = fc.showSaveDialog(MainStage.this);
            if (f != null) {
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
                    field.serialize(oos);
                    oos.flush();
                    oos.close();
                } catch (IOException ex) {
                    new Alert(Alert.AlertType.ERROR, "Greška u čuvanju datoteke! " + ex.getMessage(), ButtonType.OK).show();
                    ex.printStackTrace();
                }
            }
        });

        importSudoku.setOnAction(e -> {
            FileChooser fc = new FileChooser();
            FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("[*.sud] - Spider Sudoku fajlovi", "*.sud");
            fc.getExtensionFilters().add(filter);
            fc.setSelectedExtensionFilter(filter);
            fc.setTitle("Uvezi Sudoku");
            File f = fc.showOpenDialog(MainStage.this);
            if (f != null) {
                try {
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
                    field = Field.deserialize(ois);
                    root.setCenter(field);
                    ois.close();
                } catch (IOException ex) {
                    new Alert(Alert.AlertType.ERROR, "Greška u čitanju datoteke! " + ex.getMessage(), ButtonType.OK).show();
                    ex.printStackTrace();
                } catch (ClassCastException ex) {
                    new Alert(Alert.AlertType.ERROR, "Nekompatibilni tip datoteke!" + ex.getMessage(), ButtonType.OK).show();
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    new Alert(Alert.AlertType.ERROR, "Nepoznati tip datoteke!" + ex.getMessage(), ButtonType.OK).show();
                    ex.printStackTrace();
                }
            }
        });

        main.getItems().addAll(newSudoku, loadSudoku, new SeparatorMenuItem(), exportSudoku, importSudoku, new SeparatorMenuItem(), reloadCss);
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(main);

        Button btnPrimeni = new Button("Primeni izmene");
        btnPrimeni.setOnAction(e -> {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (!field.cells[i][j].isFinalFieldActive()) {
                        field.cells[i][j].autoActivate();
                    }
                    field.clearFieldsForCell(i, j);
                }
            }
        });

        Label lblBrojSačuvanihStanja = new Label("Broj sačuvanih stanja: 0");

        Button btnSacuvajStanje = new Button("Sačuvaj stanje");
        btnSacuvajStanje.setOnAction(e -> {
            stanja.push(field.clone());
            lblBrojSačuvanihStanja.setText("Broj sačuvanih stanja: " + stanja.size());
        });

        Button btnVrati = new Button("Vrati prethodno stanje");
        btnVrati.setOnAction(e -> {
            if (stanja.isEmpty()) {
                new Alert(Alert.AlertType.ERROR, "Nema sačuvanih stanja", ButtonType.OK).show();
            } else {
                field = stanja.pop();
                root.setCenter(field);
                lblBrojSačuvanihStanja.setText("Broj sačuvanih stanja: " + stanja.size());
            }
        });

        root.setTop(menuBar);
        root.setCenter(rootPane);

        HBox hbox = new HBox(10);
        hbox.getChildren().addAll(btnPrimeni, btnSacuvajStanje, btnVrati, lblBrojSačuvanihStanja);

        root.setBottom(hbox);

        Scene sc = new Scene(root);
        sc.getStylesheets().add(Main.class.getResource("sudoku.css").toExternalForm());

        reloadCss.setOnAction(e -> {
            sc.getStylesheets().clear();
            sc.getStylesheets().add(Main.class.getResource("sudoku.css").toExternalForm());
        });

        setScene(sc);
    }

    void loadSudoku(String sudokuString) {
        int[][] nums = new int[9][9];
        int row = 0, col = 0;
        for (char e : sudokuString.toCharArray()) {
            if (row == 9)
                break;
            if (e >= '1' && e <= '9') {
                nums[row][col] = (int) e - (int) '0';
                col++;
                if (col == 9) {
                    row += 1;
                    col = 0;
                }
            } else if (e == '0' || e == '#' || e == '*') {
                nums[row][col] = 0;
                col++;
                if (col == 9) {
                    row += 1;
                    col = 0;
                }
            }
        }
        Field sudoku = Field.withDefaultCells();

        for (row = 0; row < 9; row++) {
            for (col = 0; col < 9; col++) {
                if (nums[row][col] != 0) {
                    sudoku.cells[row][col].setValue(nums[row][col], true);
                    sudoku.clearFieldsForCell(row, col);
                }
            }
        }
        root.setCenter(sudoku);
        this.field = sudoku;
        stanja.clear();
    }


}
