package view;

import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;

import java.io.Serializable;

/**
 * Created by tomcaa on 12/9/17.
 */
public class SmallLabel extends Label implements Serializable {

    private final String[] validClasses = {"disabled", "black", "blue", "red"};
    private State state;
    private Cell parentCell;

    public SmallLabel(String value, Cell parentCell) {
        super(value);
        setState(State.ENABLED);
        getStyleClass().add("small-label");
        getStyleClass().add("c" + value);
        this.parentCell = parentCell;

        setOnMouseClicked(e -> {
            if (e.getButton() == MouseButton.PRIMARY) {
                if (state != State.DISABLED) {
                    setState(State.DISABLED);
                } else {
                    setState(State.ENABLED);
                }
            } else {
                parentCell.propagateMarkedEvent(getText(), true);
            }
        });
    }

    public void switchMarkedState(String value) {
        if (getStyleClass().contains("marked")) {
            getStyleClass().remove("marked");
        } else if (value.equals(getText()) && !getStyleClass().contains("marked") && !state.equals(State.DISABLED)) {
            getStyleClass().add("marked");
        }
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        for (String cls : validClasses) {
            if (getStyleClass().contains(cls)) {
                getStyleClass().remove(cls);
            }
        }
        this.state = state;
        if (state != State.ENABLED) {
            getStyleClass().add(state.toString().toLowerCase());
        }
        if (state == State.DISABLED && getStyleClass().contains("marked")) {
            getStyleClass().remove("marked");
        }
    }

}
