package view;

import javafx.scene.control.Label;

import java.io.Serializable;

/**
 * Created by tomcaa on 12/9/17.
 */
public class LargeLabel extends Label implements Serializable {

    private final String[] validClasses = {"black", "blue"};
    private State state;
    private Cell parent;

    public LargeLabel(final String value, final Cell parent) {
        super(value);
        this.parent = parent;
        getStyleClass().add("large-label");
        setState(State.BLUE);
        setOnMouseClicked(e -> {
            LargeLabel self = (LargeLabel) (e.getSource());
            if (!self.getState().equals(State.BLACK)) {
                parent.activateSmallFields();
            }
        });
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
        for (String cls : validClasses) {
            if (getStyleClass().contains(cls)) {
                getStyleClass().remove(cls);
            }
        }
        getStyleClass().add(state.toString().toLowerCase());
    }

}
