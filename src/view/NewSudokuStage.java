package view;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by tomcaa on 12/9/17.
 */
public class NewSudokuStage extends Stage {

    VBox vbox = new VBox(20);

    TextArea txtSudoku = new TextArea();
    Button btnUnesi = new Button("Unesi");
    MainStage parent;

    public NewSudokuStage(final MainStage parent) {
        super();
        setTitle("Unos novog sudokua");
        vbox.getChildren().addAll(txtSudoku, btnUnesi);
        setScene(new Scene(vbox));
        this.parent = parent;

        btnUnesi.setOnMouseClicked(e -> {
            parent.loadSudoku(txtSudoku.getText());
            NewSudokuStage.this.close();
        });
    }

    public void showAndWait() {
        initModality(Modality.APPLICATION_MODAL);
        show();
    }
}
