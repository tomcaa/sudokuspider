# SudokuSpider

SudokuSpider is a simple helper for solving sudoku puzzles, written in JavaFX.

## Solving Assistance Capabilities
 * It shows possible values in each field using different colors so they can be easily recognized
 * It can fill in the only possibility fields.
 * It can eliminate values based on filled-in fields
 * It has "save state" mechanics which can be used for backtracking. 

## Input / Output
 * Puzzles can be read in from keyboard input or text files. 
 * Puzzles in progress can also be exported/imported using specific object serialization mechanism (.sud files).

## Localization
It is written in Serbian (latin) locale, but might be localized in future.

## License
Copyright 2018 Milan Tomić

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.