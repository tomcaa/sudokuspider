package sample;

import javafx.application.Application;
import javafx.stage.Stage;
import view.MainStage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        MainStage mainStage = new MainStage();
        mainStage.show();
    }
}
